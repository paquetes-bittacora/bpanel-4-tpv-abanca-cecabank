<?php

declare(strict_types=1);

use App\Http\Middleware\VerifyCsrfToken;
use Bittacora\Bpanel4\Payment\CecaBank\Http\CecaBankPublicController;
use Bittacora\Bpanel4\Payment\CecaBank\Http\CecaBankResponseController;
use Illuminate\Support\Facades\Route;

Route::prefix('/tpv-ceca')->name('bpanel4-cecabank.')->middleware(['web'])
    ->group(static function (): void {
        Route::post('/respuesta-tpv', [CecaBankResponseController::class, 'readTpvResponse'])->name('process-response')
            ->withoutMiddleware([VerifyCsrfToken::class]);
        Route::get('/error-pago', [CecaBankPublicController::class, 'showUrlKo'])->name('url-ko');
        Route::get('/pago-ok', [CecaBankPublicController::class, 'showUrlOk'])->name('url-ok');
        Route::get('/formulario-tpv/{order}', [CecaBankPublicController::class, 'showForm'])->name('show-form');
    });
