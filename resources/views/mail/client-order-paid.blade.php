@extends('bpanel4-public.mail.base-mail')
<?php /**
 * @var \Bittacora\Bpanel4\Orders\Models\Order\Order $order
 */ ?>
@section('preheader')@endsection
@section('content')
    <h2>Pago confirmado</h2>
    <p>{{ $order->getClient()->getName() }}, se ha confirmado el pago de su pedido {{ $order->getId() }}.</p>
    <p>En breve comenzaremos con la preparación y el envío de su pedido.</p>
@endsection