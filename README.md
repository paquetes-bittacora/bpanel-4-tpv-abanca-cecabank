# TPV Cecabank

El plugin se instala solo, pero hay que completar algunos valores en el .env.
El paquete los añade automáticamente al final del archivo, pero los valores deben
obtenerse del TPV del cliente.

``` 
#CECA_ENV= # production o test

#CECA_MERCHANT_ID_TEST=
#CECA_ENCRYPTION_KEY_TEST=
#CECA_AQUIRER_BIN_TEST=
#CECA_URL_OK_TEST="${APP_URL}/tpv-ceca/pago-ok"
#CECA_URL_NOK_TEST="${APP_URL}/tpv-ceca/error-pago"


#CECA_MERCHANT_ID_PRODUCTION=
#CECA_ENCRYPTION_KEY_PRODUCTION=
#CECA_AQUIRER_BIN_PRODUCTION=
#CECA_URL_OK_PRODUCTION="${APP_URL}/tpv-ceca/pago-ok"
#CECA_URL_NOK_PRODUCTION="${APP_URL}/tpv-ceca/error-pago"
```

Los valores válidos para CECA_ENV son `production` o `test`, y en función de este
parámetro se usarán los valores correspondientes de CECA_MERCHANT_ID, etc.

# URL notificación del TPV

La url a la que debe llamar el TPV para confirmar las operaciones es:

```
<dominio>/tpv-ceca/respuesta-tpv
```

Esta dirección no se configura desde el módulo, sino que debe configurarse en el panel de control del TPV.