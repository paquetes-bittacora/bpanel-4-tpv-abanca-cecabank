<?php

declare(strict_types=1);

return [
    // Entorno ('test' o 'production')
    'environment'       => env('CECA_ENV', 'testing'),
    // Entorno de pruebas
    'merchant_id_test'       => env('CECA_MERCHANT_ID_TEST'),
    'encryption_key_test'    => env('CECA_ENCRYPTION_KEY_TEST'),
    'aquirer_bin_test'       => env('CECA_AQUIRER_BIN_TEST'),
    'url_ok_test'            => env('CECA_URL_OK_TEST'),
    'url_nok_test'           => env('CECA_URL_NOK_TEST'),
    // Entorno de producción
    'merchant_id_production'       => env('CECA_MERCHANT_ID_PRODUCTION'),
    'encryption_key_production'    => env('CECA_ENCRYPTION_KEY_PRODUCTION'),
    'aquirer_bin_production'       => env('CECA_AQUIRER_BIN_PRODUCTION'),
    'url_ok_production'            => env('CECA_URL_OK_PRODUCTION'),
    'url_nok_production'           => env('CECA_URL_NOK_PRODUCTION'),];
