<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Payment\CecaBank\Tests\Functional\Services;

use Bittacora\Bpanel4\Orders\Database\Factories\OrderFactory;
use Bittacora\Bpanel4\Orders\Enums\OrderStatus;
use Bittacora\Bpanel4\Payment\CecaBank\Exceptions\InvalidOperationNumberException;
use Bittacora\Bpanel4\Payment\CecaBank\Services\CecaBankOrderConfirmator;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

final class CecaBankOrderConfirmatorTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @throws InvalidOperationNumberException
     */
    public function testConfirmaUnPedido(): void
    {
        $this->artisan('bpanel4-orders:create-default-order-statuses');
        $order = (new OrderFactory())->createOne();
        $cecaBankOrderConfirmator = $this->app->make(CecaBankOrderConfirmator::class);
        $cecaBankOrderConfirmator->confirmOrder($order);

        $order->refresh();
        $this->assertEquals(OrderStatus::PAYMENT_COMPLETED->value, $order->getStatus()->id);
    }
}
