<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Payment\CecaBank\Tests\Functional\Services;

use Bittacora\Bpanel4\Payment\CecaBank\Exceptions\InvalidOperationTypeException;
use Bittacora\Bpanel4\Payment\CecaBank\Exceptions\InvalidSignatureException;
use Bittacora\Bpanel4\Payment\CecaBank\Services\CecaBankResponseValidator;
use Illuminate\Support\Facades\Config;
use Tests\TestCase;

final class CecaBankResponseValidatorTest extends TestCase
{
    /**
     * @throws InvalidSignatureException
     * @throws InvalidOperationTypeException
     */
    public function testValidaLaRespuestaDelTPV(): void
    {
        $cecaBankResponseValidator = $this->app->make(CecaBankResponseValidator::class);
        Config::set('cecabank.environment', 'test');
        Config::set('cecabank.encryption_key_test', '85bL&5O23#p4Jg@G');

        $this->assertNull($cecaBankResponseValidator->validateResponse([
            'Firma' => 'c3adee8bc90399f9a52d2b6e6fe95792630d5b82234a32a6d57d0b9100d73701',
            'MerchantID' => '1234',
            'AcquirerBIN' => '1234',
            'TerminalID' => '000003',
            'Num_operacion' => 'P023490239',
            'Importe' => '1234',
            'TipoMoneda' => '1',
            'Exponente' => '2',
            'Referencia' => 'reference-13040',
            'Tipo_operacion' => 'C',
        ]));
    }
}
