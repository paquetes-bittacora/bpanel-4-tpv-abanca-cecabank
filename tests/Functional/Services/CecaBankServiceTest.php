<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Payment\CecaBank\Tests\Functional\Services;

use Bittacora\Bpanel4\Orders\Database\Factories\OrderFactory;
use Bittacora\Bpanel4\Payment\CecaBank\Services\CecaBankService;
use Bittacora\Bpanel4\Payment\CecaBank\Services\Vendor\CecaBankTpv;
use Bittacora\Bpanel4\Prices\Types\Price;
use Illuminate\Contracts\Config\Repository;
use Illuminate\Contracts\Debug\ExceptionHandler;
use Illuminate\Contracts\View\Factory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Mockery;
use Tests\TestCase;
use Throwable;

final class CecaBankServiceTest extends TestCase
{
    use RefreshDatabase;

    private string $randomMerchantId = '';
    private string $randomString = '';

    /**
     * @throws Throwable
     */
    public function testGeneraElHtmlDelFormularioDelTpv(): void
    {
        $this->randomString = bin2hex(random_bytes(4));
        $this->randomMerchantId = (string)random_int(0, PHP_INT_MAX);
        $cecabankService = new CecaBankService(
            new CecaBankTpv(),
            $this->app->make(Factory::class),
            $this->getConfigRepository(),
            $this->app->make(ExceptionHandler::class),
        );

        $order = (new OrderFactory())->createOne();
        $amountIntegerPart = random_int(10, 200);
        $amountDecimalPart = random_int(10, 99);
        $order->setOrderTotal((new Price((float) ($amountIntegerPart .'.'. $amountDecimalPart)))->toInt());
        $html = $cecabankService->createForm($order)->render();

        $this->assertStringContainsString('name="MerchantID" value="' . $this->randomMerchantId . '"', $html);
        $this->assertStringContainsString(
            'name="AcquirerBIN" value="TEST_AQUIRER_BIN' . $this->randomString . '"',
            $html
        );
        $this->assertStringContainsString('name="URL_OK" value="TEST_URL_OK' . $this->randomString . '"', $html);
        $this->assertStringContainsString('name="URL_NOK" value="TEST_URL_NOK' . $this->randomString . '"', $html);
        $this->assertStringContainsString('name="Importe" value="' . $amountIntegerPart . $amountDecimalPart, $html);
    }

    private function getConfigRepository(): Repository
    {
        $config = Mockery::mock(Repository::class);

        $configValues = [
            'cecabank.environment' => 'test',
            'cecabank.merchant_id_test' => $this->randomMerchantId,
            'cecabank.encryption_key_test' => 'TEST_ENCRYPTION_KEY' . $this->randomString,
            'cecabank.aquirer_bin_test' => 'TEST_AQUIRER_BIN' . $this->randomString,
            'cecabank.url_ok_test' => 'TEST_URL_OK' . $this->randomString,
            'cecabank.url_nok_test' => 'TEST_URL_NOK' . $this->randomString,
        ];

        foreach ($configValues as $key => $value) {
            $config->shouldReceive('get')->with($key)->andReturn($value);
        }

        return $config;
    }
}
