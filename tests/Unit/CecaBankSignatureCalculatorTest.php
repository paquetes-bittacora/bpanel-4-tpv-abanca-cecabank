<?php

declare(strict_types=1);

namespace Unit;

use Bittacora\Bpanel4\Payment\CecaBank\Services\CecaBankSignatureCalculator;
use Tests\TestCase;

final class CecaBankSignatureCalculatorTest extends TestCase
{
    /**
     * Los datos y el resultado son fijos, este test fallará si cambia el número de parámetros, etc de la función, o
     * si alguna versión futra de PHP hace que haya que actualizar la clase. En general, el cálculo de la firma no
     * cambiará.
     */
    public function testCalculaLafirma(): void
    {
        $result = CecaBankSignatureCalculator::calculateSignature(
            '85bL&5O23#p4Jg@G',
            '1234',
            '1234',
            '000003',
            'P023490239',
            '1234',
            '1',
            '2',
            'reference-13040'
        );

        $this->assertEquals('c3adee8bc90399f9a52d2b6e6fe95792630d5b82234a32a6d57d0b9100d73701', $result);
    }
}
