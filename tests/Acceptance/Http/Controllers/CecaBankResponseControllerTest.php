<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Payment\CecaBank\Tests\Acceptance\Http\Controllers;

use Tests\TestCase;

final class CecaBankResponseControllerTest extends TestCase
{
    /**
     * Solo compruebo que carga y devuelve la cadena de fallo que indican desde Ceca, ya que para que devuelva la
     * correcta habría que simular datos y funcionamientos que ya se comprueban en otros tests.
     */
    public function testLaDireccionDeRespuestaDelTpvCarga(): void
    {
        // Pongo la dirección manualmente para que falle si la cambiamos, ya que esta dirección normalmente irá
        // también en la configuración propia del TPV
        $response = $this->post('/tpv-ceca/respuesta-tpv');
        $response->assertSuccessful();
        $response->assertContent('$*$NOK$*$');
    }
}
