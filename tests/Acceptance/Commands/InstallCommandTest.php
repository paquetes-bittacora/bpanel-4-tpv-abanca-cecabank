<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Payment\CecaBank\Tests\Acceptance\Commands;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

final class InstallCommandTest extends TestCase
{
    use RefreshDatabase;

    public function testElComandoDeInstalacionSeEjecuta(): void
    {
        /** @phpstan-ignore-next-line */
        $this->artisan('bpanel4-cecabank:install')->assertSuccessful();
    }
}
