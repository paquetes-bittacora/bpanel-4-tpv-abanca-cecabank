<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Payment\CecaBank\Commands;

use Bittacora\Bpanel4\Payment\CecaBank\Bpanel4CecaBankServiceProvider;
use Bittacora\Bpanel4\Payment\CecaBank\PaymentMethods\Cecabank;
use Bittacora\Bpanel4\Payment\Payment;
use Illuminate\Console\Command;
use Illuminate\Contracts\Console\Kernel;
use Illuminate\Foundation\Application;
use RuntimeException;

final class InstallCommand extends Command
{
    /** @var string $signature */
    protected $signature = 'bpanel4-cecabank:install';

    public function handle(Kernel $artisan, Application $app): void
    {
        $artisan->call('vendor:publish', [
            '--provider' => Bpanel4CecaBankServiceProvider::class,
        ]);

        $this->addEnvConfigEntries($app);

        $paymentModule = $app->make(Payment::class);
        $paymentMethod = $app->make(Cecabank::class);
        $paymentModule->registerPaymentMethod($paymentMethod);
    }

    private function addEnvConfigEntries(Application $app): void
    {
        $env = $app->environment();
        $basePath = $app->basePath();
        $file = in_array($env, ['production', 'local']) ? $basePath . '/.env' : $basePath . '/.env.' . $env;
        $fileContents = file_get_contents($file);

        if (false === $fileContents) {
            throw new RuntimeException('No se pudo leer el archivo .env');
        }

        $string = "\n# bPanel4 CecaBank ------------------------------
#CECA_ENV=test # production o test

#CECA_MERCHANT_ID_TEST=
#CECA_ENCRYPTION_KEY_TEST=
#CECA_AQUIRER_BIN_TEST=
#CECA_URL_OK_TEST=\"${APP_URL}/tpv-ceca/pago-ok\"
#CECA_URL_NOK_TEST=\"${APP_URL}/tpv-ceca/error-pago\"

#CECA_MERCHANT_ID_PRODUCTION=
#CECA_ENCRYPTION_KEY_PRODUCTION=
#CECA_AQUIRER_BIN_PRODUCTION=
#CECA_URL_OK_PRODUCTION=\"${APP_URL}/tpv-ceca/pago-ok\"
#CECA_URL_NOK_PRODUCTION=\"${APP_URL}/tpv-ceca/error-pago\"
# \bPanel4 CecaBank ------------------------------
";

        if (!str_contains($fileContents, 'bPanel4 CecaBank')) {
            file_put_contents($file, $string, FILE_APPEND);
        }
    }
}
