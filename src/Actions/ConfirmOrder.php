<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Payment\CecaBank\Actions;

use Bittacora\Bpanel4\Payment\CecaBank\Exceptions\InvalidOperationNumberException;
use Bittacora\Bpanel4\Payment\CecaBank\Exceptions\InvalidOperationTypeException;
use Bittacora\Bpanel4\Payment\CecaBank\Exceptions\InvalidSignatureException;
use Bittacora\Bpanel4\Payment\CecaBank\Mail\OrderPaidAdminMail;
use Bittacora\Bpanel4\Payment\CecaBank\Mail\OrderPaidClientMail;
use Bittacora\Bpanel4\Payment\CecaBank\Services\CecabankOperationNumberParser;
use Bittacora\Bpanel4\Payment\CecaBank\Services\CecaBankOrderConfirmator;
use Bittacora\Bpanel4\Payment\CecaBank\Services\CecaBankResponseValidator;
use Illuminate\Contracts\Config\Repository;
use Illuminate\Contracts\Mail\Mailer;

final class ConfirmOrder
{
    public function __construct(
        private readonly CecaBankResponseValidator $cecaBankResponseValidator,
        private readonly CecaBankOrderConfirmator $cecaBankOrderConfirmator,
        private readonly CecabankOperationNumberParser $cecabankOperationNumberParser,
        private readonly Mailer $mailer,
        private readonly Repository $configRepository,
    ) {
    }

    /**
     * @param array<string, string> $requestData
     * @throws InvalidOperationNumberException
     * @throws InvalidOperationTypeException
     * @throws InvalidSignatureException
     */
    public function execute(array $requestData): void
    {
        $this->cecaBankResponseValidator->validateResponse($requestData);
        $order = $this->cecabankOperationNumberParser->getOrderFromTpvRequest($requestData);
        $this->cecaBankOrderConfirmator->confirmOrder($order);
        $this->mailer->to($order->getClient()->getEmail())->send(new OrderPaidClientMail($order));
        
        $adminEmail = $this->configRepository->get('bpanel4.admin_email');

        if ($this->configRepository->has('orders.orders_admin_email')) {
            $adminEmail = $this->configRepository->get('orders.orders_admin_email');
        }

        $this->mailer->to($adminEmail)->send(new OrderPaidAdminMail($order));        
    }
}
