<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Payment\CecaBank\Mail;

use Bittacora\Bpanel4\Orders\Models\Order\Order;
use Illuminate\Mail\Mailable;

final class OrderPaidAdminMail extends Mailable
{
    public function __construct(private readonly Order $order)
    {
    }

    public function build(): OrderPaidAdminMail
    {
        return $this->subject('Se ha recibido el pago por tarjeta del pedido' . $this->order->getId())
            ->view('bpanel4-payment-cecabank::mail.admin-order-paid', [
                'client' => $this->order->getClient(),
                'order' => $this->order,
            ]);
    }
}
