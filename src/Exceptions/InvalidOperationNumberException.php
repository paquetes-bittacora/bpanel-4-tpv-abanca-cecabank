<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Payment\CecaBank\Exceptions;

use Exception;

final class InvalidOperationNumberException extends Exception
{
    /** @var mixed $message */
    protected $message = 'El número de operación no es válido';
}
