<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Payment\CecaBank\Exceptions;

use Exception;

final class InvalidSignatureException extends Exception
{
    /** @var mixed $message */
    protected $message = 'La firma del TPV no es válida';
}
