<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Payment\CecaBank;

use Bittacora\Bpanel4\Payment\CecaBank\Commands\InstallCommand;
use Illuminate\Support\ServiceProvider;

final class Bpanel4CecaBankServiceProvider extends ServiceProvider
{
    private const PACKAGE_PREFIX = 'bpanel4-payment-cecabank';

    public function boot(): void
    {
        $this->commands([InstallCommand::class]);
        $this->loadViewsFrom(__DIR__ . '/../resources/views', self::PACKAGE_PREFIX);
        $this->publishes([__DIR__.'/../config/cecabank.php' => $this->app->configPath('cecabank.php')]);
        $this->loadRoutesFrom(__DIR__ . '/../routes/web.php');
    }
}
