<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Payment\CecaBank\Services;

use Bittacora\Bpanel4\Orders\Models\Order\Order;
use Bittacora\Bpanel4\Payment\CecaBank\Enums\Environment;
use Bittacora\Bpanel4\Payment\CecaBank\Services\Vendor\CecaBankTpv;
use Bittacora\Bpanel4\Prices\Types\Price;
use Exception;
use Illuminate\Contracts\Config\Repository;
use Illuminate\Contracts\Debug\ExceptionHandler;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Throwable;
use Webmozart\Assert\Assert;

final class CecaBankService
{
    public function __construct(
        private readonly CecaBankTpv $tpv,
        private readonly Factory $view,
        private readonly Repository $config,
        private readonly ExceptionHandler $exceptionHandler,
    ) {
    }

    /**
     * @throws Throwable
     */
    public function createForm(Order $order): View
    {
        try {
            $this->tpv->setEntorno();
            $this->tpv->setMerchantID($this->getConfigValue('cecabank.merchant_id'));
            $this->tpv->setClaveEncriptacion($this->getConfigValue('cecabank.encryption_key'));
            $this->tpv->setAcquirerBIN($this->getConfigValue('cecabank.aquirer_bin'));
            $this->tpv->setUrlOk($this->getConfigValue('cecabank.url_ok'));
            $this->tpv->setUrlNok($this->getConfigValue('cecabank.url_nok'));
            $this->tpv->setNumOperacion('P' . str_pad((string) $order->getId(), 6, '0', STR_PAD_LEFT));
            $this->tpv->setImporte($this->convertAmount($order->getOrderTotal()));
            $this->setTpvEnvironment();
            $this->tpv->setSubmit();
            return $this->view->make('bpanel4-payment-cecabank::tpv-form', [
                'form' => $this->tpv->create_form_with_redirection(),
            ]);
        } catch (Exception $e) {
            $this->exceptionHandler->report($e);
            throw $e;
        }
    }

    private function getConfigValue(string $key): string
    {
        $suffix = $this->getEnvironment();
        $value = $this->config->get($key . '_' . $suffix->value);
        Assert::string($value);
        return $value;
    }

    private function convertAmount(float $amount): string
    {
        $orderTotal = Price::fromInt((int) $amount);
        return number_format($orderTotal->toFloat(), 2, ',', '');
    }

    private function getEnvironment(): Environment
    {
        $environment = $this->config->get('cecabank.environment');
        Assert::string($environment);
        return Environment::from($environment);
    }

    private function setTpvEnvironment(): void
    {
        if (Environment::PRODUCTION === $this->getEnvironment()) {
            $this->tpv->setEntorno('produccion');
            return;
        }
        $this->tpv->setEntorno('desarrollo');
    }
}
