<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Payment\CecaBank\Services;

final class CecaBankSignatureCalculator
{
    public static function calculateSignature(
        string $encryptionKey,
        string $merchantId,
        string $aquirerBin,
        string $terminalId,
        string $operationNumber,
        string $amount,
        string $currencyType,
        string $exponent,
        string $reference,
    ): string {
        $args = [...func_get_args()];
        return strtolower(hash('sha256', implode('', $args)));
    }
}
