<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Payment\CecaBank\Services;

use Bittacora\Bpanel4\Payment\CecaBank\Enums\Environment;
use Bittacora\Bpanel4\Payment\CecaBank\Enums\OperationType;
use Bittacora\Bpanel4\Payment\CecaBank\Exceptions\InvalidOperationTypeException;
use Bittacora\Bpanel4\Payment\CecaBank\Exceptions\InvalidSignatureException;
use Illuminate\Contracts\Config\Repository;
use Webmozart\Assert\Assert;

/**
 * Valida la respuesta enviada por el TPV al hacer un pago
 */
final class CecaBankResponseValidator
{
    public function __construct(private readonly Repository $configRepository)
    {
    }

    /**
     * @param array<string, string> $data
     * @throws InvalidSignatureException
     * @throws InvalidOperationTypeException
     */
    public function validateResponse(array $data): void
    {
        $this->validateSignature($data['Firma'], $data);
        $this->validateOperationType($data['Tipo_operacion']);
    }

    /**
     * @param array<string, string> $data
     * @throws InvalidSignatureException
     */
    private function validateSignature(string $expectedSignature, array $data): void
    {
        $signature = CecaBankSignatureCalculator::calculateSignature(
            $this->getEncryptionKey(),
            $data['MerchantID'],
            $data['AcquirerBIN'],
            $data['TerminalID'],
            $data['Num_operacion'],
            $data['Importe'],
            $data['TipoMoneda'],
            $data['Exponente'],
            $data['Referencia'],
        );

        if ($signature !== $expectedSignature) {
            throw new InvalidSignatureException();
        }
    }

    private function getEncryptionKey(): string
    {
        $configEnvironment = $this->configRepository->get('cecabank.environment');
        Assert::string($configEnvironment);
        $environment = Environment::from($configEnvironment);
        $tpvEnvironment = $this->configRepository->get('cecabank.encryption_key_' . $environment->value);
        Assert::string($tpvEnvironment);
        return $tpvEnvironment;
    }

    /**
     * Por ahora solo aceptamos pagos normales por tarjeta y bizum.
     * @throws InvalidOperationTypeException
     */
    private function validateOperationType(string $operationTypeString): void
    {
        $operationType = OperationType::from($operationTypeString);

        if (OperationType::REGULAR_PAYMENT !== $operationType && OperationType::BIZUM_PAYMENT !== $operationType) {
            throw new InvalidOperationTypeException();
        }
    }
}
