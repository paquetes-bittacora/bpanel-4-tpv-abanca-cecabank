<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Payment\CecaBank\Services;

use Bittacora\Bpanel4\Orders\Models\Order\Order;
use Bittacora\Bpanel4\Payment\CecaBank\Exceptions\InvalidOperationNumberException;

final class CecabankOperationNumberParser
{
    /**
     * @param array<string, string> $data
     * @throws InvalidOperationNumberException
     */
    public function getOrderFromTpvRequest(array $data): Order
    {
        return $this->getOrder($this->parseOperationNumber($data['Num_operacion']));
    }

    /**
     * @throws InvalidOperationNumberException
     */
    private function parseOperationNumber(string $operationNumber): int
    {
        if (1 !== preg_match('/P\d+/', $operationNumber)) {
            throw new InvalidOperationNumberException();
        }

        return (int) substr($operationNumber, 1);
    }

    private function getOrder(int $orderId): Order
    {
        return Order::whereId($orderId)->firstOrFail();
    }
}
