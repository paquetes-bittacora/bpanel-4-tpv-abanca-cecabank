<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Payment\CecaBank\Services;

use Bittacora\Bpanel4\Orders\Enums\OrderStatus;
use Bittacora\Bpanel4\Orders\Models\Order\Order;
use Bittacora\OrderStatus\Models\OrderStatusModel;

final class CecaBankOrderConfirmator
{
    public function confirmOrder(Order $order): void
    {
        $order->setStatus(OrderStatusModel::whereId(OrderStatus::PAYMENT_COMPLETED->value)->firstOrFail());
        $order->save();
    }
}
