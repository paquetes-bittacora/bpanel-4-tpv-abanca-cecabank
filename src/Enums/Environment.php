<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Payment\CecaBank\Enums;

enum Environment: string
{
    case PRODUCTION = 'production';
    case TEST = 'test';
}
