<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Payment\CecaBank\Enums;

enum OperationType: string
{
    case AMEX_PAYMENT = 'A';
    case REGULAR_PAYMENT = 'C';
    case BIZUM_PAYMENT = 'E';
    case BIZUM_AUTHENTICATION_ONLY = 'E0';
    case PREAUTH_OR_NOT_COUNTABLE_AUTH = 'G';
    case CHARGE_FOR_AUTHORIZATION_OR_PRESENTATION = 'H';
}
