<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Payment\CecaBank\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * @method array<string, string> all($keys = null)
 */
final class TpvRequest extends FormRequest
{
    /**
     * @phpstan-return array<void>
     */
    public function rules(): array
    {
        return [];
    }
}
