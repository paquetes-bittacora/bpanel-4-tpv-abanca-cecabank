<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Payment\CecaBank\Http;

use App\Http\Controllers\Controller;
use Bittacora\Bpanel4\Orders\Models\Order\Order;
use Bittacora\Bpanel4\Payment\CecaBank\Services\CecaBankService;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Illuminate\View\Factory;

final class CecaBankPublicController extends Controller
{
    public function __construct(private readonly Factory $view)
    {
    }

    public function showForm(Request $request, Order $order, CecaBankService $cecaBankService): View
    {
        return $cecaBankService->createForm($order);
    }

    public function showUrlKo(): View
    {
        return $this->view->make('bpanel4-payment-cecabank::url-ko');
    }

    public function showUrlOk(): View
    {
        return $this->view->make('bpanel4-payment-cecabank::url-ok');
    }
}
