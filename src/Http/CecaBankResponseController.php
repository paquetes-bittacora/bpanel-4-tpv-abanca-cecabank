<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Payment\CecaBank\Http;

use Bittacora\Bpanel4\Payment\CecaBank\Actions\ConfirmOrder;
use Bittacora\Bpanel4\Payment\CecaBank\Requests\TpvRequest;
use Illuminate\Contracts\Debug\ExceptionHandler;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Throwable;

final class CecaBankResponseController extends Controller
{
    public function __construct(private readonly ExceptionHandler $exceptionHandler)
    {
    }

    /**
     * A este método sólo llamará la ruta a la que llamará el TPV para confirmar una operación.
     * @throws Throwable
     */
    public function readTpvResponse(
        TpvRequest $request,
        ConfirmOrder $confirmOrder,
    ): Response {
        try {
            $confirmOrder->execute($request->all());
            return new Response('$*$OKY$*$');
        } catch (Throwable $e) {
            $this->exceptionHandler->report($e);
            return new Response('$*$NOK$*$');
        }
    }
}
